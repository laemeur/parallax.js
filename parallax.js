/* parallax.js - version M45.1003
   
   Very simple 2.5D parallax scroller scriptie. 

   Attaches to <diorama> elements in the document and makes their
   child elements pan around relative to the position of the pointer.

   If the child elements have a "depth" attribute, ranging from 0-1,
   this will determine the amount that they are moved vertically and
   horizontally relative to one another: "1" means that the element
   appears nearest to the viewer (moves the most), "0" means that the
   element does not move at all. If no "depth" attribute is present,
   then the scrolling/panning amount is computed automatically based
   on the document order of the element.

   Although this script attaches to ALL <diorama> elements in a page,
   I think the best usage is to construct single dioramas in simple
   HTML documents, then using <object> or <embed> to contain/crop each
   diorama as an image in a larger document.


   LICENSE

   This code is licensed under the current version of the GNU Public
   License <http://fsf.org/...>. Use, modify, and redistribute freely;
   go make something neat with it.


   TO-DO: 

   - Still need to properly adjust size and placement of layers,
     according to whether the image and/or browser is portrait- or
     landscape- oriented. Landscape-oriented (or square) images work
     fine right now in a landscape-oriented browser, but I honestly
     haven't tested the opposite cases.

   - Layer rotation? Probably outside the scope of this script, but
     I'm imagining a fanning-cards effect that might be fun to play 
     with. 

   ©2019,2022 LÆMEUR <adam@laemeur.com>.

*/

var sn = 1.57;

function mousemv(e){
    if(e.currentTarget){
	var diorama = e.currentTarget;
    } else {
	// Touch event
	var diorama =  container(e.target,"diorama");
    }
    var maxPan = parseFloat(diorama.getAttribute("maxPan")) || 0.1;
    var maxScroll = parseFloat(diorama.getAttribute("maxScroll")) || 0.05;
    
    var drect = diorama.getBoundingClientRect();

    /* I would PREFER to use offsetX and offsetY, but those are not available
       with touch events?! */
    
    //var xpos = e.offsetX;
    //var ypos = e.offsetY;

    //var xpos = e.screenX - drect.left;
    //var ypos = e.screenY - drect.top;

    var xpos = e.clientX - drect.left;
    var ypos = e.clientY - drect.top;

    var layers = Array.from(diorama.children);

    // Determine orientation
    var orientation = "landscape";
    if(diorama.hasAttribute("orientation")){
	orientation = diorama.getAttribute("orientation").toLowerCase();
    } else if (drect.width < drect.height) {
	orientation = "portrait";
    }
    
    
    for(var l in layers){

	var panFactor = 0;
	var scrollFactor = 0;

	// Determine the relative amount of panning/scrolling to apply to the layer

	if (layers[l].hasAttribute("depth")) {
	    panFactor = maxPan * parseFloat(layers[l].getAttribute("depth"));
	    scrollFactor = maxScroll * parseFloat(layers[l].getAttribute("depth"));
	} else {
	    panFactor = maxPan * (1 - Math.cos((1 / layers.length) * l * sn));
	    scrollFactor = maxScroll * (1 - Math.cos((1 / layers.length) * l * sn));	    
	}

	
	// Scale the layer so that its edges don't show when moved to the maximumum
	// scroll/pan position.

	if(orientation == "landscape"){
	    // Landscape
	    layers[l].style.height = ( (1 + maxScroll) * drect.height ) + "px";
	    layers[l].style.width = "";
	} else {
	    // Portrait
	    layers[l].style.width = ( (1 + maxPan) * drect.width ) + "px";
	    layers[l].style.height = "";
	}


	// Pan (left/right) layer:
	var centeredXpos = -0.5 + (xpos / drect.width);
	var leftOffset =  centeredXpos * drect.width * panFactor * maxPan;
	var scaleCompensation = (
	    ( ( 1 + ((orientation == "landscape") ? maxScroll : maxPan )) *
				    drect.width ) - drect.width ) / 2;
	layers[l].style.left = leftOffset
	    - scaleCompensation + window.scrollX + "px";
	
	/* Experimental 3D rotation of layers...

	layers[l].style.transform = "rotate3d(0, 1, 0, " +
	    (centeredXpos * 45 * panFactor) + "deg)";

	*/

	// Scroll (up/down) layer:
	var centeredYpos = -0.5 + (ypos / drect.height); 
	var topOffset =  centeredYpos * drect.height * scrollFactor * maxScroll;
	var yScaleCompensation = ( ( (
		1 + (( orientation == "landscape" ) ? maxScroll : maxPan ))
				     * drect.height) - drect.height ) / 2;
	layers[l].style.top = topOffset
	    - yScaleCompensation + window.scrollY + "px";
	
    }
}

function container(el,tag){
    
    /* Hunt up the DOM for the containing element of "tag" type. */
    
    while(el.tagName.toLowerCase() != tag.toLowerCase() && el != document.body){
	el = el.parentElement;
    }
    return el;
}


function mousedown(e){
    
    /* We don't want the diorama layers to be picked-up. */

    e.preventDefault();
}

function mousedrag(e){
    e.preventDefault();
    e.stopPropagation();
}

function touchStart(e){
    e.preventDefault();
}

function touchMove(e){
    e.preventDefault();
    mousemv(e.touches.item(0));
}

function mouseEnter(e){
    
    /* Turn on CSS transitions so that there's not such a jarring "snap"
       to the new cursor location, then set a timeout to turn it 'em back off. */

    var d = e.currentTarget;
    var layers = Array.from(d.children);
    for (l in layers){
	
	layers[l].style.transition = "all 0.5s";
    
	setTimeout(function(){
	    this.style.transition = "";
	}.bind(layers[l]), 500);
    }
    
}

function init(){
    
    /* Attaches the script to all <diorama> elements in the document. */

    var dioramas = Array.from(document.getElementsByTagName("diorama"));

    for (var d in dioramas){
	dioramas[d].addEventListener('drag',mousedrag);
	dioramas[d].addEventListener('mousedown',mousedown);
	dioramas[d].addEventListener('mousemove',mousemv);
	dioramas[d].addEventListener('touchmove',touchMove);
	dioramas[d].addEventListener('touchstart',touchStart);
	dioramas[d].addEventListener('mouseenter',mouseEnter);
    }
    
}

window.addEventListener('load',init);

